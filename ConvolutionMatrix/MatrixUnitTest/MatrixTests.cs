﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ConvolutionMatrix;

namespace MatrixUnitTest
{
    [TestClass]
    public class MatrixTests
    {
        [TestMethod]
        public void TestMethod1()
        {
            SMatrix matrixa = new SMatrix(8, 10);
            for (int i = 0; i < matrixa.arr.GetLength(0); i++)
            {
                for (int j = 0; j < matrixa.arr.GetLength(1); j++)
                {
                    matrixa.arr[i,j] = j+1;
                }
            }

            SMatrix matrixb = new SMatrix(3, 3);
            for (int a = 0; a < matrixb.arr.GetLength(0); a++)
            {
                for (int b = 0; b < matrixb.arr.GetLength(1); b++)
                {
                    matrixb.arr[a, b] = b + 1;
                }
            }

            SMatrix matrixactual = matrixa * matrixb;

            SMatrix matrixexpected = new SMatrix(6, 8);
            for (int c = 0; c < matrixexpected.arr.GetLength(0); c++)
            {
                for (int d = 0; d < matrixexpected.arr.GetLength(1); d++)
                {
                    int summe = 0;

                    for (int e = 0; e < 3; e++)
                    {
                        for (int f = 0; f < 3; f++)
                        {
                            summe += matrixa.arr[c + e, d + f] * matrixb.arr[e, f];
                        }
                    }
                    matrixexpected.arr[c, d] = summe;
                }
            }

            CollectionAssert.AreEqual(matrixexpected.arr, matrixactual.arr);
        }
    }
}
