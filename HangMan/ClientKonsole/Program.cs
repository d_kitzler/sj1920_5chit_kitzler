﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using HangMan;

namespace ClientKonsole
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Client client = new Client();
            client.GetConnection(IPAddress.Loopback, 1337);

            client.GameOver += GameOver;
            client.attemptsUpdated += CurrentMistake;
            client.WordUpdated += WordUpdated;

            client.Start();

            while(true)
            {
                Console.WriteLine("Geben Sie ein Instruct ein: [letter or newGame]");

                switch(Console.ReadLine())
                {
                    case "letter":
                        char l;
                        Console.WriteLine("Geben Sie einen Buchstaben ein: ");

                        if (Char.TryParse(Console.ReadLine(), out l))
                        {
                            await client.GuessTheLetter(l);
                        }

                        else
                        {
                            Console.WriteLine("Falsche Eingabe!!!");
                        }

                        break;

                    case "newGame":
                        int maxAttempts;
                        Console.WriteLine("Maximale Versuche eingeben: ");
                        if (Int32.TryParse(Console.ReadLine(), out maxAttempts))
                        {
                            await client.NewGame(maxAttempts);
                        }

                        else
                        {
                            Console.WriteLine("Falsche Eingabe!!!");
                        }

                        break;
                }
            }
        }

        private static void GameOver(string word)
        {
            Console.WriteLine("GAME OVER! WORD: "+word);
        }

        private static void WordUpdated(string word)
        {
            Console.WriteLine("WORD UPDATED! WORD: " + word);
        }

        private static void CurrentMistake(int mistakes)
        {
            Console.WriteLine("Current Mistakes are: "+mistakes);
        }
    }
}
