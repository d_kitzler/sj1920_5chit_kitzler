﻿using System;
using System.IO;
using iText.IO.Font;
using iText.IO.Image;
using iText.Kernel.Font;
using iText.Kernel.Pdf;
using iText.Layout;
using iText.Layout.Element;

namespace iTextShartPrototyp
{
    class Program
    {
        static void Main(string[] args)
        {
            string PIC = "logo.png";
            PdfDocument pdfDocument = new PdfDocument(new PdfWriter(new FileStream("hello.pdf", FileMode.Create, FileAccess.Write)));
            Document document = new Document(pdfDocument);

            String line = "Hello! Welcome to iTextPdf";

            var font = PdfFontFactory.CreateFont(FontConstants.TIMES_ROMAN);

            List list = new List().SetSymbolIndent(12).SetListSymbol("-").SetFont(font);
            // Add ListItem objects
            list.Add(new ListItem("Never gonna give you up"))
                .Add(new ListItem("Never gonna let you down"))
                .Add(new ListItem("Never gonna run around and desert you"))
                .Add(new ListItem("Never gonna make you cry"))
                .Add(new ListItem("Never gonna say goodbye"))
                .Add(new ListItem("Never gonna tell a lie and hurt you"));

            iText.Layout.Element.Image fox = new Image(ImageDataFactory.Create(PIC));

            string line_ = "THIS is my Protoyp of iText";

            document.Add(new Paragraph(line));
            document.Add(new Paragraph(line_));

            document.Add(list);
            document.Add(fox);

           

            document.Close();
            Console.WriteLine("Awesome PDF just got created.");


        }
    }
}
