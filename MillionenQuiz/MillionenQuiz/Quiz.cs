﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MillionenQuiz
{
    class Quiz
    {
        public static int Gid { get; set; } = 0;
        public static Dictionary<int, Quiz> quizDic = new Dictionary<int, Quiz>();
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public List<Fragen> Fragens = new List<Fragen>();
        List<List<Fragen>> alleFragen;
        public int Level { get; set; }
        public Quiz(List<List<Fragen>> qe)
        {
            alleFragen = qe;
            Gid++;
            Start = DateTime.Now;
            quizDic.Add(Gid, this);
        }
        public Fragen GetFragen(int level)
        {
            Fragen q = new Fragen();
            List<Fragen> qlist;
            Random randy = new Random();
            if (level == 0)
                qlist = alleFragen[0];
            else if (level == 1)
                qlist = alleFragen[1];
            else if (level == 2)
                qlist = alleFragen[2];
            else
                qlist = null;
            Level = level;
            q = qlist[randy.Next(qlist.Count)];
            Fragens.Add(q);
            return q;
        }
        public void EndGame() => End = DateTime.Now;
    }
}
