﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IronPdf;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using MySql.Data.MySqlClient;

namespace RechnungPDFGenerator
{
    class RechnungCreater
    {
        Rechnung rechnung = new Rechnung();
        MySqlConnection con = null;

        public RechnungCreater(string database, string root, string password)
        {
            con = new MySqlConnection($"Server=localhost;Database={database};Uid={root};Pwd={password};");
        }

        public List<(int ID, string name)> ReadAllRechnungen()
        {
            con.Open();
            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = con;
            cmd.CommandText = "select ID, Name from rechnung";
            MySqlDataReader reader = cmd.ExecuteReader();
            List<(int ID, string name)> help = new List<(int ID, string name)>();
            while (reader.Read())
            {
                var help2 = (i: 0, s: "");
                help2.i = Convert.ToInt32(reader[0]);
                help2.s = reader[1].ToString();
                help.Add(help2);
            }

            reader.Close();
            con.Close();
            return help;
        }
        public void ReadCreateRechnung(int ID)
        {
            HtmlToPdf Renderer = new HtmlToPdf();
            Renderer.PrintOptions.PaperSize = PdfPrintOptions.PdfPaperSize.A4;
            Renderer.PrintOptions.PaperOrientation = PdfPrintOptions.PdfPaperOrientation.Portrait;

            con.Open();
            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = con;
            cmd.CommandText = $"SELECT * FROM rechnung where id={ID}";
            MySqlDataReader reader = cmd.ExecuteReader();

            int kunden_id = 0;
            int firmen_id = 0;
            int zk_id = 0;

            while (reader.Read())
            {
                rechnung.Nummer = reader.GetInt32(0);
                rechnung.Name = reader.GetString(1);
                rechnung.BeginText = reader.GetString(2);
                rechnung.EndText = reader.GetString(3);
                rechnung.Rabatt = reader.GetInt32(4);
                kunden_id = reader.GetInt32(5);
                firmen_id = reader.GetInt32(6);
                zk_id = reader.GetInt32(7);
                rechnung.FooterText = reader.GetString(8);
                rechnung.Projekttext = reader.GetString(9);
                rechnung.Sachberater = reader.GetString(10);
            }
            reader.Close();
            con.Close();

            rechnung.Firma = ReadFirma(firmen_id);
            rechnung.Kunde = ReadKunde(kunden_id);
            rechnung.Zahlungskonditionen = ReadZK(zk_id);

            con.Open();
            cmd.CommandText = $"select * from rechnungen_artikel where RechnungID={ID}";
            List<int> artikeln = new List<int>();
            reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                artikeln.Add(Convert.ToInt32(reader[0]));
            }

            reader.Close();
            con.Close();

            List<Artikel> articles = new List<Artikel>();
            foreach (int i in artikeln)
                articles.Add(ReadArtikel(i));
            rechnung.artikelListe = articles;
            string articletext = "";
            int counter = 0;
            double summenetto = 0;
            double summe13 = 0, summe20 = 0;
            foreach (Artikel a in articles)
            {
                counter++;
                articletext += "<tr style=\"word-wrap:break-word;vertical-align: top; text-align: left;\"><td style=\"padding-bottom:20px;\">" + counter + "</td><td style=\"padding-bottom:20px;\">" + a.Bezeichnung + "</td><td style=\"padding-bottom:20px;text-align:right;\">" + a.Anzahl + "</td><td style=\"padding-bottom:20px;\">" + a.Einheit + "</td><td style=\"padding-bottom:20px;text-align:right;\">" + a.Preis.ToString("n2") + "</td><td style=\"padding-bottom:20px;text-align:right;\">-" + a.Rabatt + "%</td><td style=\"padding-bottom:20px;text-align:right;\">" + (a.Preis * a.Anzahl - a.Preis * a.Anzahl * (a.Rabatt / 100)).ToString("n2") + "</td></tr>";
                summenetto += a.Preis * a.Anzahl - (a.Preis * a.Anzahl * (a.Rabatt / 100));
                if (a.Ust == 13)
                    summe13 += a.Preis * a.Anzahl;
                else if (a.Ust == 20)
                    summe20 += a.Preis * a.Anzahl;
            }

            Renderer.PrintOptions.Footer = new SimpleHeaderFooter() { CenterText = rechnung.FooterText, DrawDividerLine = true, RightText = "Seite {page} von {total-pages}" };

            string firstpart = "<meta charset=\"UTF-8\"><body style=\"font-family:arial;\"><header><address style=\"float:right\"><p style=\"font-size:20px;\">" + rechnung.Firma.Name + "<br>" + rechnung.Firma.Strasse + "<br>" + rechnung.Firma.PLZ + " " + rechnung.Firma.Ort + "</p></address</header>" +
             "<article style=\"padding-top:100px;\"><address><p>" + rechnung.Kunde.Nachname + "<br>" + rechnung.Kunde.Vorname + "</p><p>" + rechnung.Kunde.Strasse + " " + "<br>" + rechnung.Kunde.PLZ + " " + rechnung.Kunde.Ort + "</p></address>" +
             "<p style=\"font-size:26px;\"><b>Rechnung - " + DateTime.Now.Year + "/" + ID + "</b><br><span style=\"font-size:20px;\">" + rechnung.Projekttext + "</span></p>" +
             "<p style=\"float:right; margin-top:-80px; font-size:16px; text-align:right;\">" + DateTime.Now.Date.ToShortDateString() + "<br>" + rechnung.Sachberater + "</p>" +
             "<p>" + rechnung.BeginText + "</p>" +
             "<table style=\"width:100%; border-bottom: 1px solid black; text-align: left;border-collapse: collapse;table-layout:fixed;\" cellpadding=\"2\">" +
             "<tr style=\"border-bottom: 1px solid black; word-wrap:break-word;vertical-align: top; text-align: left;\"><th>Pos</th><th style=\"width:40%\">Text</th><th style=\"text-align:center;\">Menge</th><th style=\"text-align:left;\">Eh</th><th style=\"width:12%;text-align:right;\">Preis/Eh</th><th style=\"text-align:right;\">Rab</th><th style=\"width:12%; text-align:right;\">Netto</th></tr>" +
             articletext +
             "</table>" +
             "<p>Zahlungskonditionen:<br>" + rechnung.Zahlungskonditionen.Prozent + "% Rabatt bei Zahlung bis " + rechnung.Zahlungskonditionen.Tage + ",<br>Zahlungsziel bis " + rechnung.Zahlungskonditionen.TageNetto + "</p>" +
             "<p style=\"float:right; margin-top:-70px; border-bottom:1px solid black; width:275px;\"><span style=\"float:left;\">Summe-Netto</span><span style=\"float:right;\">" + summenetto.ToString("n2") + "</span><br>" +
             "<span style=\"float:left;\">abzgl. Rabatt -" + rechnung.Rabatt + "%</span><span style=\"float:right;\">" + (summenetto * (rechnung.Rabatt / 100)).ToString("n2") + "</span><br>" +
             "<span style=\"float:left;\">abzgl. Summe</span><span style=\"float:right;\">" + (summenetto - summenetto * (rechnung.Rabatt / 100)).ToString("n2") + "</span><br>" +
             "<span style=\"float:left;\">+13% Ust. von " + summe13.ToString("n2") + "</span><span style=\"float:right;\">" + (summe13 * 0.13).ToString("n2") + "</span><br>" +
             "<span style=\"float:left;\">+20% Ust. von " + summe20.ToString("n2") + "</span><span style=\"float:right;\">" + (summe20 * 0.2).ToString("n2") + "</span></p>" +
             "<p style=\"float:right; margin-top:30px; margin-right: -275px; width:275px;\"><span style=\"float:left;\">Endbetrag</span><span style=\"float:right;\">" + ((summenetto - summenetto * (rechnung.Rabatt / 100)) + summe13 * 0.13 + summe20 * 0.2).ToString("n2") + "</span></p>" +
             "<p style=\"margin-top:120px;\">" + rechnung.EndText + "<br><br>Mit freundlichen Grüßen<br>" + rechnung.Firma.Name + "</p>" +
             "</article></body>";

            Renderer.RenderHtmlAsPdf(firstpart).SaveAs("Rechnung_" + ID + ".pdf");
            string text = "";
            string table = "<table style=\"width:100%; border-bottom: 1px solid black;text-align: left;border-collapse: collapse;table-layout:fixed;\" cellpadding=\"2\">" +
                "<tr style=\"border-bottom: 1px solid black; word-wrap:break-word;vertical-align: top; text-align: left;\"><th>Pos</th><th style=\"width:40%\">Text</th><th style=\"text-align:center;\">Menge</th><th style=\"text-align:left;\">Eh</th><th style=\"width:12%;text-align:right;\">Preis/Eh</th><th style=\"text-align:right;\">Rab</th><th style=\"width:12%; text-align:right;\">Netto</th></tr>";
            PdfReader preader = new PdfReader("Rechnung_" + ID + ".pdf");

            if (preader.NumberOfPages == 2)
            {
                text = PdfTextExtractor.GetTextFromPage(preader, 2); preader.Close();
                if (text.Contains("Zahlungskonditionen:"))
                {
                    string helptext = text;
                    text = text.Substring(0, text.IndexOf("Zahlungskonditionen:"));

                    IronPdf.PdfDocument PDF = IronPdf.PdfDocument.FromFile("Rechnung_" + ID + ".pdf");
                    PDF.RemovePage(1);
                    string tablecontent = "";
                    if (text != "")
                    {
                        if (Char.IsDigit(text[0]) && Char.IsDigit(text[1]))
                        {
                            tablecontent = firstpart.Substring(firstpart.IndexOf("<tr style=\"word-wrap:break-word;vertical-align: top; text-align: left;\"><td style=\"padding-bottom:20px;\">" + text[0] + "" + text[1]));
                        }
                        else if (Char.IsDigit(text[0]))
                        {
                            tablecontent = firstpart.Substring(firstpart.IndexOf("<tr style=\"word-wrap:break-word;vertical-align: top; text-align: left;\"><td style=\"padding-bottom:20px;\">" + text[0]));
                        }
                        else
                            tablecontent = firstpart.Substring(firstpart.IndexOf("</table>"));
                    }
                    else
                    {
                        table = "";
                        tablecontent = firstpart.Substring(firstpart.IndexOf(helptext.Split(new char[] { ' ', '\n' })[0]));
                    }
                    string addBill = "<body style=\"font-family:arial;\">" +
                        "<article><p style=\"font-size:20px;\"><span style=\"font-size:26px;\"><b>Rechnung - " + DateTime.Now.Year + "/" + ID + "</b></span><br>" + rechnung.Kunde.Nachname + " " + rechnung.Kunde.Vorname + "<br>" + rechnung.BeginText + "</p>" +
                        table +
                        tablecontent +
                        "</article>" +
                        "</body>";
                    var Renderer2 = new HtmlToPdf();
                    Renderer2.PrintOptions.Footer.DrawDividerLine = true;
                    Renderer2.PrintOptions.Footer.FontSize = 10;
                    //Renderer2.PrintOptions.Footer.RightText = "Seite 2 von 2";


                    Renderer.PrintOptions.Footer = new SimpleHeaderFooter() { CenterText = rechnung.FooterText, DrawDividerLine = true, RightText = "Seite {page} von {total-pages}" };
                    Renderer2.PrintOptions.Footer = new SimpleHeaderFooter() { CenterText = rechnung.FooterText, DrawDividerLine = true, RightText = "Seite 2 von 2" };

                    IronPdf.PdfDocument.Merge(PDF, Renderer2.RenderHtmlAsPdf(addBill)).SaveAs("Rechnung_" + ID + ".pdf");

                }
            }

            Process.Start("Rechnung_" + ID + ".pdf");
        }
        public Kunde ReadKunde(int ID)
        {
            con.Open();
            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = con;
            cmd.CommandText = $"select * from kunde where ID={ID}";
            Kunde k = new Kunde();
            MySqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                k.ID = ID;
                k.Vorname = reader.GetString(1);
                k.Nachname = reader.GetString(2);
                k.PLZ = reader.GetInt32(4);
                k.Strasse = reader.GetString(3);
                k.Ort = reader.GetString(5);
            }

            reader.Close();
            con.Close();
            return k;
        }
        public Firma ReadFirma(int ID)
        {
            con.Open();
            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = con;
            cmd.CommandText = $"select * from firma where ID={ID}";
            MySqlDataReader reader = cmd.ExecuteReader();
            Firma f = new Firma();
            while (reader.Read())
            {
                f.ID = ID;
                f.Name = reader[1].ToString();
                f.Strasse = reader[2].ToString();
                f.PLZ = reader[3].ToString();
                f.Ort = reader[4].ToString();

            }

            reader.Close();
            con.Close();
            return f;
        }
        public Zahlungskonditionen ReadZK(int ID)
        {
            con.Open();
            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = con;
            cmd.CommandText = $"select * from zahlungskonditionen where ID={ID}";
            MySqlDataReader reader = cmd.ExecuteReader();
            Zahlungskonditionen z = new Zahlungskonditionen();
            while(reader.Read())
            {
                z.ID = ID;
                z.Prozent = reader.GetInt32(1);
                z.Tage = reader.GetInt32(2);
                z.TageNetto = reader.GetInt32(3);
            }

            reader.Close();
            con.Close();
            return z;
        }
        public Artikel ReadArtikel(int ID)
        {
            con.Open();
            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = con;
            cmd.CommandText = $"select * from artikel where ID={ID}";
            Artikel a = new Artikel();
            MySqlDataReader reader = cmd.ExecuteReader();
            int rechnung_id = 0;

            while(reader.Read())
            {
                a.ID = ID;
                a.Preis = reader.GetDouble(1);
                a.Bezeichnung = reader.GetString(2);
                a.Anzahl = reader.GetInt32(3);
                a.Einheit = reader.GetString(4);
                a.Rabatt = reader.GetDouble(5);
                rechnung_id = reader.GetInt32(6);
                a.Ust = reader.GetDouble(7);
                a.Datum = reader.GetString(8);
            }

            reader.Close();
            con.Close();
            return a;
        }
    }
}
