﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RechnungPDFGenerator
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        RechnungCreater r;
        List<(int ID, string Name)> list;

        Dictionary<int, string> help = new Dictionary<int, string>();

        public MainWindow()
        {
            r = new RechnungCreater("rechnungen", "root", "");
            InitializeComponent();
            list = r.ReadAllRechnungen();

            foreach (var i in list)
                help.Add(i.ID, i.Name);

            cBox.ItemsSource = help.Keys;

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
           r.ReadCreateRechnung((int)cBox.SelectedItem);
        }
    }
}
