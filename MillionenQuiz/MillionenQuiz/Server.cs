﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace MillionenQuiz
{
    class Server
    {
        TcpListener server;
        List<Verbindung> connections;
        public Server()
        {
            server = new TcpListener(IPAddress.Parse("127.0.0.1"), 3333);
            connections = new List<Verbindung>();
        }

        public List<Verbindung> Connections
        {
            get { return connections; }
            set { connections = value; }
        }

        public void Anfragen()
        {
            while (true)
            {          
                Socket s = server.AcceptSocket();
                neueVerbindung(s);
            }
        }

        public void neueVerbindung(Socket s)
        {
            Verbindung c = new Verbindung(s);
            Connections.Add(c);
        }

        public void Start()
        {
            server.Start();
        }

        public void Stop()
        {
            foreach (Verbindung c in Connections)
                c.StopService();
            server.Stop();
        }
    }
}

