﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace HangMan
{
    /// <summary> 
    ///This is the class "Connection". The class "Connection" connects the server with the client.
    /// </summary>

    public class Connection
    {
        private string word;
        private int mistakeCounter;
        private WordProvider wordProvider;
        private StreamReader streamReader;
        private StreamWriter streamWriter;
        private Stream s;
        private Socket socket;
        private int maximumAttemps;
        private Task task;

        private bool exit;

        public Connection(string filename, Socket socket)
        {
            wordProvider = WordProvider.GetInstance(filename);
            task = new Task(GetDataAsync);

            this.socket = socket;

            task.Start();
        }

        public async Task SendDataAsync(string xmlData)
        {
            await streamWriter.WriteLineAsync(xmlData);
            await streamWriter.FlushAsync();
        }       

        public void NewGame(int maxAttempts)
        {
            this.maximumAttemps = maxAttempts;
            word = wordProvider.GetWord();
            mistakeCounter = 0;

        }

        public void Stop()
        {
            if (task.Status == TaskStatus.Running)
                this.exit = true;

            //streamWriter.Close();
            //streamReader.Close();
            //s.Close();
        }

        /// <summary>       
        /// Here the method gets the data asynchrono.It is queried in a switch which command was entered by the client and then wid executed the following case
        /// </summary>
        public async void GetDataAsync()
        {      
            while(true)
            {
                s = new NetworkStream(socket);
                streamReader = new StreamReader(s);
                streamWriter = new StreamWriter(s);

                string dataReceive = "";
                StringBuilder stringBuilder = new StringBuilder();

                while(streamReader.Peek() != -1)
                {
                    stringBuilder.Append(await streamReader.ReadLineAsync());
                }

                dataReceive = stringBuilder.ToString();

                if(dataReceive != "")
                {
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(dataReceive);

                    XmlNode hangman = doc.SelectSingleNode("Hangman");
                    XmlAttribute attribute = hangman.Attributes["instruct"];

                    string instruction = attribute.Value;

                    switch(instruction)
                    {                     
                        case "new":
                            XmlNode maximumA = doc.SelectSingleNode("Hangman/Attempts");
                            NewGame(Convert.ToInt32(maximumA.InnerText));
                            XmlDocument send = new XmlDocument();
                            send.LoadXml("<Hangman instruct=\"newGame\"><Length>" + this.word.Length + "</Length></Hangman>");
                            XmlNode length = send.SelectSingleNode("Hangman/Length");
                            length.InnerText = word.Length.ToString();
                            await SendDataAsync(send.OuterXml);
                            break;

                        case "letter":
                            XmlNode letter_ = doc.SelectSingleNode("Hangman/Letter");
                            char letter = Convert.ToChar(letter_.InnerText);

                            bool guess = false;

                            List<int> attemptsList = new List<int>();

                            if (word.ToLower().IndexOf(letter) != -1)
                            {
                                guess = true;
                                int index = 0;
                                while (word.ToLower().IndexOf(letter, index) != -1)
                                {
                                    attemptsList.Add(word.ToLower().IndexOf(letter, index));
                                    index = word.ToLower().IndexOf(letter, index)+1;
                                }
                            }

                            else
                            {
                                mistakeCounter++;
                            }

                            if (mistakeCounter <= this.maximumAttemps)
                            {
                                XmlDocument sendLetter = new XmlDocument();
                                sendLetter.LoadXml("<Hangman instruct=\"letter\"><Letter>" + letter + "</Letter><Success></Success><CurrentMistakes></CurrentMistakes><Positions></Positions></Hangman>");
                                sendLetter.SelectSingleNode("Hangman/CurrentMistakes").InnerText = mistakeCounter.ToString();
                                if (guess == true)
                                {
                                    sendLetter.SelectSingleNode("Hangman/Success").InnerText = "TRUE";
                                }

                                else if (guess == false)
                                {
                                    sendLetter.SelectSingleNode("Hangman/Success").InnerText = "FALSE";

                                }

                                foreach (int num in attemptsList)
                                {
                                    XmlNode position = sendLetter.CreateNode(XmlNodeType.Element, "Position", null);
                                    position.InnerText = num.ToString();
                                    sendLetter.SelectSingleNode("Hangman/Positions").AppendChild(position);
                                }

                                await SendDataAsync(sendLetter.OuterXml);

                            }

                            else
                            {
                                XmlDocument sendLetterDoc = new XmlDocument();
                                sendLetterDoc.LoadXml("<Hangman instruct =\"gameOver\"><Word>" + word + "</Word></Hangman>");
                            }

                            break;
                    }
                }
            }
        }
    }
}