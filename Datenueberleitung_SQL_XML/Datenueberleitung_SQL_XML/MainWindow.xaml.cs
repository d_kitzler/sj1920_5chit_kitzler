﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;

namespace Datenueberleitung_SQL_XML
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string filename;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void openButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();

            openFileDialog.DefaultExt = "accdb";
            openFileDialog.Filter = "ACCDB Files (*.accdb)|*.accdb";

            openFileDialog.ShowDialog();

            filename = openFileDialog.FileName.ToString();

            ReadIn(filename);
        }

        private void ReadIn(string filename)
        {
            string cs = @"Provider = Microsoft.ACE.OLEDB.12.0; Data Source = " + filename + "; Persist Security Info = False;";
            OleDbConnection connection = new OleDbConnection(cs);
            connection.Open();
            OleDbCommand command = new OleDbCommand();
            command.Connection = connection;

            command.CommandText = "select count(Nachname) from patient";
            int anzahlPatienten = (int) command.ExecuteScalar();
            tb1.Text = anzahlPatienten.ToString();

            command.CommandText = "select count(PatientID) from PatientBehandlungen";
            int anzahlBehandlung = (int)command.ExecuteScalar();
            tb2.Text = anzahlBehandlung.ToString();

            command.CommandText = "SELECT sum(Dauer) FROM PatientBehandlungen";
            tb7.Text = command.ExecuteScalar().ToString();

            command.CommandText = "SELECT count(id) FROM behandlung";
            tb3.Text = command.ExecuteScalar().ToString();

            command.CommandText = "SELECT COUNT(ID) FROM (SELECT COUNT(BehandlungsID) as c, PatientID as ID FROM PatientBehandlungen GROUP BY PatientID) WHERE c = 1;";
            tb4.Text = command.ExecuteScalar().ToString();

            command.CommandText = "SELECT COUNT(ID) FROM (SELECT COUNT(BehandlungsID) as c, PatientID as ID FROM PatientBehandlungen GROUP BY PatientID) WHERE c = 2;";
            tb5.Text = command.ExecuteScalar().ToString();

            command.CommandText = "SELECT COUNT(ID) FROM (SELECT COUNT(BehandlungsID) as c, PatientID as ID FROM PatientBehandlungen GROUP BY PatientID) WHERE c > 2;";
            tb6.Text = command.ExecuteScalar().ToString();

        }
        private string selectPatients()
        {
            string cs = "Provider = Microsoft.Ace.OLEDB.12.0; Data Source = Transfer.accdb";

            OleDbConnection con = new OleDbConnection(cs);
            con.Open();
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = con;
            cmd.CommandText = "select * from Patient";
            OleDbDataReader reader = cmd.ExecuteReader();
            StringBuilder patienten = new StringBuilder();

            while (reader.Read())
            {
                patienten.Append(reader[0] + "-" + reader[1] + "-" + reader[2] + "-" + reader[3] + "\n");
            }

            reader.Close();
            con.Close();
            return patienten.ToString();
        }

        private void transferButton_Click(object sender, RoutedEventArgs e)
        {
            string[] patientlist = selectPatients().Split('\n');
            string[] bPatientlist = selectPatientenBehandlung().Split('\n');
            string[] behandlunglist = selectBehandlungen().Split('\n');

            XmlDocument doc = new XmlDocument();
            doc.LoadXml("<Liste Export=\"07.12.2012\"></Liste>");

            XmlNode root = doc.SelectSingleNode("Liste");

            foreach (string s in patientlist)
            {
                XmlNode p = doc.CreateElement("Name");
                string[] helpP = s.Split('-');

                if (helpP.Length == 4)
                {
                    XmlNode nachname = doc.CreateElement("Nachname"); nachname.InnerText = helpP[2];
                    XmlNode vorname = doc.CreateElement("Vorname"); vorname.InnerText = helpP[1];
                    XmlNode sv = doc.CreateElement("SV"); sv.InnerText = helpP[3];
                    //XmlNode behandlugen = doc.CreateElement("Behandlungen"); behandlugen.InnerText = helpP[4];
                    XmlAttribute id = doc.CreateAttribute("ID"); id.InnerText = helpP[0];
                    p.AppendChild(nachname);
                    p.AppendChild(vorname);
                    p.AppendChild(sv);
                    //p.AppendChild(behandlugen);
                    p.Attributes.Append(id);
                    root.AppendChild(p);
                }

                XmlNode bh = doc.CreateElement("Behandlungen");
                foreach (string t in bPatientlist)
                {
                    string[] helpPB = t.Split('-');
                    if (helpPB.Length == 4)
                    {
                        if (helpPB[0] == helpP[0])
                        {
                            XmlNode behandlung = doc.CreateElement("Behandlung");
                            XmlNode bz = doc.CreateElement("Bezeichnung");
                            foreach (string u in behandlunglist)
                            {
                                string[] helpB = u.Split('-');
                                if (helpPB[1] == helpB[0])
                                    bz.InnerText = helpB[1];
                            }
                            XmlNode da = doc.CreateElement("Dauer"); 
                            da.InnerText = helpPB[2];
                            XmlNode dt = doc.CreateElement("Datum"); 
                            dt.InnerText = Convert.ToDateTime(helpPB[3]).ToShortDateString();
                            behandlung.AppendChild(bz);
                            behandlung.AppendChild(da);
                            behandlung.AppendChild(dt);
                            bh.AppendChild(behandlung);
                        }
                    }
                }
                p.AppendChild(bh);
            }
            doc.Save("Transfer.xml");

            int patienten = selectPatients().Split('\n').Count() - 1;
            int behandlungen = selectPatientenBehandlung().Split('\n').Count() - 1;
            int verschiedeneBehanlungen = selectBehandlungen().Split('\n').Count() - 1;

            int anzahlEineBehandlung = 0;
            int anzahlZweiBehandlungen = 0;
            int anzahlMehrAlsZweiBehandlungen = 0;
            int summeTherapieZeit = 0;

            List<string> patientenID = new List<string>();
            List<string> patientenBehandlungen = selectPatientenBehandlung().Split('\n').ToList();
            patientenBehandlungen.RemoveAt(patientenBehandlungen.Count - 1);

            foreach (string s in patientenBehandlungen)
            {
                patientenID.Add(s.Split('-')[0]);
                summeTherapieZeit += Convert.ToInt32(s.Split('-')[2]);
            }

            for (int i = 0; i < patientenID.Count - 1; i++)
            {
                if (patientenID[i] == patientenID[i + 1])
                {
                    if (i < patientenID.Count - 2)
                    {
                        if (patientenID[i] == patientenID[i + 2])
                        {
                            i += 2;
                            anzahlMehrAlsZweiBehandlungen++;
                        }
                        else
                        {
                            i++;
                            anzahlZweiBehandlungen++;
                        }
                    }
                    else
                    {
                        i++;
                        anzahlZweiBehandlungen++;
                    }
                }
                else
                    anzahlEineBehandlung++;

            }

            tbsql1.Text = patienten.ToString();
            tbsql2.Text = behandlungen.ToString();
            tbsql3.Text = verschiedeneBehanlungen.ToString();
            tbsql4.Text = anzahlEineBehandlung.ToString();
            tbsql5.Text = anzahlZweiBehandlungen.ToString();
            tbsql6.Text = anzahlMehrAlsZweiBehandlungen.ToString();
            tbsql7.Text = summeTherapieZeit.ToString();

            if (tbsql1.Text == tb1.Text && tbsql2.Text == tb2.Text && tbsql3.Text == tb3.Text && tbsql4.Text == tb4.Text && tbsql5.Text == tb5.Text && tbsql6.Text == tb6.Text && tbsql7.Text == tb7.Text)
            {

                korrekt.Background = new SolidColorBrush(Colors.Green);
                korrekt.Text = "KORREKT!";
            }

            else
            {
                korrekt.Background = new SolidColorBrush(Colors.Red);
                korrekt.Text = "NICHT KORREKT!";
            }
        }
    
       private string selectBehandlungen()
        {
            string cs = "Provider = Microsoft.Ace.OLEDB.12.0; Data Source = Transfer.accdb";

            OleDbConnection con = new OleDbConnection(cs);
            con.Open();
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = con;
            cmd.CommandText = "select * from behandlung";
            OleDbDataReader reader = cmd.ExecuteReader();
            StringBuilder behandlung = new StringBuilder();

            while (reader.Read())
            {
                behandlung.Append(reader[0] + "-" + reader[1] + "\n");
            }
            reader.Close();

            con.Close();
            return behandlung.ToString();
        }

       private string selectPatientenBehandlung()
        {
            string cs = "Provider = Microsoft.Ace.OLEDB.12.0; Data Source = Transfer.accdb";

            OleDbConnection con = new OleDbConnection(cs);
            con.Open();
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = con;
            cmd.CommandText = "select * from patientbehandlungen";
            OleDbDataReader reader = cmd.ExecuteReader();
            StringBuilder patientenBehanldung = new StringBuilder();

            while (reader.Read())
            {
                patientenBehanldung.Append(reader[0] + "-" + reader[1] + "-" + reader[2] + "-" + reader[3] + "\n");
            }
            reader.Close();

            con.Close();
            return patientenBehanldung.ToString();
        }
    }
}
