﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MillionenQuiz
{
    class Fragen
    {
        public string frage { get; set; }
        public List<string> antwort = new List<string>();
        public int richtig { get; set; }
    }
}
