﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace MillionenQuiz
{
    class Verbindung
    {
        public bool Error { get; set; } = false;
        public int Level { get; set; } = 0;
        Quiz quiz;
        private Socket socket;
        private Stream stream;
        private Task receiveT;
        StreamReader sr;
        StreamWriter sw;
        public Verbindung(Socket s)
        {
            socket = s;
            receiveT = new Task(getDataAsync);
            receiveT.Start();
        }
        public async void getDataAsync()
        {
            try
            {
                while (true)
                {
                    stream = new NetworkStream(socket);
                    sr = new StreamReader(stream);
                    sw = new StreamWriter(stream);
                    string data = "";
                    StringBuilder sb = new StringBuilder();
                    while (sr.Peek() != -1)
                        sb.Append(await sr.ReadLineAsync()); 
                    data = sb.ToString();

                    if (data != "") 
                    {
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(data);
                        XmlNode root = doc.SelectSingleNode("RESPONSE"); 
                        XmlNode info = root.SelectSingleNode("ID"); 
                        int op = Convert.ToInt32(info.InnerText);

                        if (op == 1)
                        {
                            quiz = new Quiz(getFragen());
                            XmlDocument doc2 = new XmlDocument();
                            Fragen q = quiz.GetFragen(Level);
                            doc2.LoadXml("<RESPONSE><ID>1</ID><GAMEID>" + Quiz.Gid + "</GAMEID><LEVEL>" + Level + "</LEVEL><QUESTION>" + q.frage + "</QUESTION><ANSWER1>" + q.antwort[0] + "</ANSWER1><ANSWER2>" + q.antwort[1] + "</ANSWER2><ANSWER3>" + q.antwort[2] + "</ANSWER3><ANSWER4>" + q.antwort[3] + "</ANSWER4></RESPONSE>");
                            await sendDataAsync(doc2.OuterXml);
                        }

                        else if (op == 2) 
                        {
                            XmlNode id = root.SelectSingleNode("GAMEID");
                            int gid = Convert.ToInt32(id.InnerText);
                            quiz = Quiz.quizDic[gid];
                            Level++;
                            XmlDocument doc2 = new XmlDocument();
                            Fragen q = quiz.GetFragen(Level);
                            doc2.LoadXml("<RESPONSE><ID>2</ID><GAMEID>" + gid + "</GAMEID><LEVEL>" + Level + "</LEVEL><QUESTION>" + q.frage + "</QUESTION><ANSWER1>" + q.antwort[0] + "</ANSWER1><ANSWER2>" + q.antwort[1] + "</ANSWER2><ANSWER3>" + q.antwort[2] + "</ANSWER3><ANSWER4>" + q.antwort[3] + "</ANSWER4></RESPONSE>");
                            await sendDataAsync(doc2.OuterXml);
                        }
                        else if (op == 3)
                        {
                            XmlNode id = root.SelectSingleNode("GAMEID");
                            int gid = Convert.ToInt32(id.InnerText);
                            XmlNode answer_id = root.SelectSingleNode("ANSWERID");
                            int answerid = Convert.ToInt32(answer_id.InnerText);
                            quiz = Quiz.quizDic[gid];
                            int correctid = quiz.Fragens[quiz.Fragens.Count].richtig;
                            quiz.Level = Level;
                            if (answerid != correctid)
                                Error = true;
                            XmlDocument doc2 = new XmlDocument();
                            doc2.LoadXml("<RESPONSE><ID>3</ID><GAMEID>" + gid + "</GAMEID><CORRECT>" + (!Error).ToString() + "</CORRECT></RESPONSE>");
                            await sendDataAsync(doc2.OuterXml);
                        }
                        
                        else if (op == 4)
                        {
                            XmlNode id = root.SelectSingleNode("GAMEID");
                            int gid = Convert.ToInt32(id.InnerText);
                            quiz = Quiz.quizDic[gid];
                            quiz.EndGame();
                            XmlDocument doc2 = new XmlDocument();
                            doc2.LoadXml("<RESPONSE><ID>4</ID><GAMEID>" + gid + "</GAMEID><BEGIN>" + quiz.Start.ToShortTimeString() + "</BEGIN><END>" + quiz.End.ToShortTimeString() + "</END></RESPONSE>");
                            await sendDataAsync(doc2.OuterXml);
                        }
                    }
                }
            }
            catch (Exception e) { } 
        }
        List<List<Fragen>> getFragen()
        {
            List<List<Fragen>> ret = new List<List<Fragen>>();
            if (File.Exists("fragen.txt"))
            {
                string[] values = File.ReadAllLines("fragen.txt");
                List<Fragen> list0 = new List<Fragen>();
                List<Fragen> list1 = new List<Fragen>();
                List<Fragen> list2 = new List<Fragen>();

                foreach (string s in values)
                {
                    string[] help = s.Split(';');
                    Fragen q = new Fragen() { frage = help[1], antwort = new List<string>() { help[2], help[3], help[4], help[5] }, richtig = Convert.ToInt32(help[6]) };
                    if (Convert.ToInt32(help[0]) == 0)
                        list0.Add(q);
                    else if (Convert.ToInt32(help[0]) == 1)
                        list1.Add(q);
                    else if (Convert.ToInt32(help[0]) == 2)
                        list2.Add(q);
                }
                ret.Add(list0);
                ret.Add(list1);
                ret.Add(list2);

                return ret;
            }
            else
            {
                StopService();
                return null;
            }
        }
        public async Task sendDataAsync(string data)
        {
            await sw.WriteLineAsync(data);
            await sw.FlushAsync();
        }

        public void StopService()
        {
            socket.Close();
            sw.Close();
            sr.Close();
            stream.Close();
        }
    }
}

