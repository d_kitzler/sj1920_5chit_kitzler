﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Xml;

namespace HangMan
{
    /// <summary>
    /// Here is the Client Class, with the associated Classes, Events and Delegates
    /// </summary>
    public class Client
    {
        private Task task;

        private string word;
        private int attempts;

        private bool exit;

        private TcpClient client;
        private NetworkStream s;

        public delegate void AttemptsCounterDelegate(int attempts);
        public event AttemptsCounterDelegate attemptsUpdated;

        public delegate void WordUpdatedDelegate(string word);
        public event WordUpdatedDelegate WordUpdated;

        public delegate void GameOverDelegate(string word);
        public event GameOverDelegate GameOver;

        private StreamReader streamReader;
        private StreamWriter streamWriter;

        public Client()
        {
            client = new TcpClient();
        }

        public void Start()
        {
            if (task.Status != TaskStatus.Running)
                task.Start();
        }
        public void GetConnection(IPAddress ip, int port)
        {
            if (client.Connected == false)
                client.Connect(ip, port);

            s = client.GetStream();
            
            task = new Task(GetDataAsync);
        }

        public void CloseConnetion()
        {
            this.exit = true;
            streamReader.Close();
            streamWriter.Close();
            s.Close();
        }

        public async Task SendDataAsync(string dataXml)
        {
            await streamWriter.WriteLineAsync(dataXml);
            await streamWriter.FlushAsync();
        }

        /// <summary>       
        /// Here the method gets the data asynchrono.It is queried in a switch which command was entered by the client and then wid executed the following case
        /// </summary>
        public void GetDataAsync()
        {
            while (true)
            {
                streamReader = new StreamReader(s);
                streamWriter = new StreamWriter(s);
                StringBuilder stringBuilder = new StringBuilder();
                string data = "";

                while (streamReader.Peek() != -1)
                {
                    stringBuilder.Append(streamReader.ReadLine());
                }

                data = stringBuilder.ToString();

                if (data != "")
                {
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(data);

                    XmlNode hangman = doc.SelectSingleNode("Hangman");
                    XmlAttribute command = hangman.Attributes["instruct"];
                    string command_ = command.Value;

                    switch (command_)
                    {
                        case "newGame":
                            XmlNode length = doc.SelectSingleNode("Hangman/Length");
                            StringBuilder wordB = new StringBuilder();
                            for (int i = 0; i < Convert.ToInt32(length.InnerText); i++)
                            {
                                wordB.Append("_");
                            }

                            this.word = wordB.ToString();
                            this.attempts = 0;
                            this.WordUpdated.Invoke(this.word);
                            this.attemptsUpdated.Invoke(this.attempts);
                            break;

                        case "letter":
                            char l;
                            bool s;

                            XmlNode lNode = doc.SelectSingleNode("Hangman/Letter");
                            l = Convert.ToChar(lNode.InnerText);
                            XmlNode sNode = doc.SelectSingleNode("Hangman/Success");
                            s = Convert.ToBoolean(sNode.InnerText);
                            XmlNode attempts = doc.SelectSingleNode("Hangman/CurrentMistakes");
                            this.attempts = Convert.ToInt32(attempts.InnerText);

                            XmlNodeList xmlNodeList = doc.SelectNodes("Hangman/Positions/Position");
                            foreach (XmlNode positionNode in xmlNodeList)
                            {
                                char[] charArr = word.ToCharArray();
                                charArr[Convert.ToInt32(positionNode.InnerText)] = l;
                                this.word = new string(charArr);
                            }
                            this.attemptsUpdated(this.attempts);

                            this.WordUpdated(this.word);
                            break;

                        case "gameOver":
                            XmlNode wNode = doc.SelectSingleNode("Hangman/Word");
                            if (this.GameOver != null)
                            {
                                this.GameOver.Invoke(wNode.ToString());
                            }
                            break;

                    }
                }

            }
        }
        public void WordUpdate()
        {
            throw new System.NotImplementedException();
        }

        public async Task NewGame(int mistakes)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml("<Hangman instruct=\"new\"><Attempts>" + mistakes + "</Attempts></Hangman>");
            await SendDataAsync(doc.OuterXml);
        }

        public async Task GuessTheLetter(char buchstabe)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml("<Hangman instruct=\"letter\"><Letter>" + buchstabe + "</Letter></Hangman>");
            await SendDataAsync(doc.OuterXml);
        }
    }
}