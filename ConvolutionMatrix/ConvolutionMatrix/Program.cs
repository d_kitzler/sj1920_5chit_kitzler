﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvolutionMatrix
{
    class Program
    {
        static void Main(string[] args)
        {
            //normal SMatrix
            SMatrix m1 = new SMatrix(3, 4);
            m1.arr[0, 0] = 1;
            m1.arr[0, 1] = 2;
            m1.arr[0, 2] = 3;
            m1.arr[0, 3] = 4;
            m1.arr[1, 0] = 5;
            m1.arr[1, 1] = 6;
            m1.arr[1, 2] = 7;
            m1.arr[1, 3] = 8;
            m1.arr[2, 0] = 9;
            m1.arr[2, 1] = 10;
            m1.arr[2, 2] = 11;
            m1.arr[2, 3] = 12;

            SMatrix m2 = new SMatrix(2, 2);
            m2.arr[0, 0] = 2;
            m2.arr[0, 1] = 2;
            m2.arr[1, 0] = 2;
            m2.arr[1, 1] = 2;

            Stopwatch sw = new Stopwatch();
            sw.Start();
            SMatrix m3 = m1 * m2;
            sw.Stop();

            for (int i = 0; i < m3.arr.GetLength(0); i++)
            {
                for (int j = 0; j < m3.arr.GetLength(1); j++)
                {
                    Console.WriteLine(m3.arr[i,j]+" ");
                }            
            }

            Console.WriteLine("Zeit für die Berechnung: "+ sw.Elapsed.ToString());
            Console.WriteLine();


            //RGB Matrix
            RGBMatrix rgbmatrix = new RGBMatrix(2, 3, 3);
            rgbmatrix.arr[0, 0, 0] = 1;
            rgbmatrix.arr[0, 0, 1] = 2;
            rgbmatrix.arr[0, 0, 2] = 3;
            rgbmatrix.arr[0, 1, 0] = 4;
            rgbmatrix.arr[0, 1, 1] = 5;
            rgbmatrix.arr[0, 1, 2] = 6;
            rgbmatrix.arr[0, 2, 0] = 7;
            rgbmatrix.arr[0, 2, 1] = 8;
            rgbmatrix.arr[0, 2, 2] = 9;
            rgbmatrix.arr[1, 0, 0] = 1;
            rgbmatrix.arr[1, 0, 1] = 2;
            rgbmatrix.arr[1, 0, 2] = 3;
            rgbmatrix.arr[1, 1, 0] = 4;
            rgbmatrix.arr[1, 1, 1] = 5;
            rgbmatrix.arr[1, 1, 2] = 6;
            rgbmatrix.arr[1, 2, 0] = 7;
            rgbmatrix.arr[1, 2, 1] = 8;
            rgbmatrix.arr[1, 2, 2] = 9;

            SMatrix rgberg = rgbmatrix * m2;

            for (int i = 0; i < rgbmatrix.arr.GetLength(0); i++)
            {
                for (int j = 0; j < rgbmatrix.arr.GetLength(1); j++)
                {
                    Console.WriteLine(m3.arr[i, j] + " ");
                }
            }


            //BIG MATRIX
            Random randy = new Random();
            SMatrix bigmatrix = new SMatrix(11000, 11000);

            for (int i = 0; i < bigmatrix.arr.GetLength(0); i++)
            {
                for (int j = 0; j < bigmatrix.arr.GetLength(1); j++)
                {
                    bigmatrix.arr[i, j] = randy.Next(0, 10);
                }
            }

            sw.Reset();
            sw.Start();
            SMatrix bigerg = bigmatrix * m2;
            sw.Stop();
            Console.WriteLine("Zeit für die Berechnung der großen Matrix: " + sw.Elapsed.ToString());

            //KLEINER Matrix
            SMatrix smallermatrix = new SMatrix(1100, 1100);

            for (int i = 0; i < smallermatrix.arr.GetLength(0); i++)
            {
                for (int j = 0; j < smallermatrix.arr.GetLength(1); j++)
                {
                    smallermatrix.arr[i, j] = randy.Next(0, 10);
                }
            }

            sw.Reset();
            sw.Start();
            SMatrix smallerg = smallermatrix * m2;
            sw.Stop();
            Console.WriteLine("Zeit für die Berechnung der kleineren Matrix: " + sw.Elapsed.ToString());

            Console.ReadLine();
        }
    }
}
