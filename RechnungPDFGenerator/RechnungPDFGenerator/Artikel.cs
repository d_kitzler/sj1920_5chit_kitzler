﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RechnungPDFGenerator
{
    class Artikel
    {
        public string Bezeichnung { get; set; }

        public string Einheit { get; set; }

        public string Datum { get; set; }

        public double Rabatt { get; set; }

        public double Preis { get; set; }

        public double Ust { get; set; }

        public int ID { get; set; }

        public int Anzahl { get; set; }

    }
}
