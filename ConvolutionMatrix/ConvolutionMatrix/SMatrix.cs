﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvolutionMatrix
{
    public class SMatrix
    {
        public int[,] arr;

        public SMatrix(int x, int y)
        {
            arr = new int[x, y];
        }

        public SMatrix(int[,] arr)
        {
            this.arr = arr;
        }
        public static SMatrix operator* (SMatrix a, SMatrix b)
        {
            SMatrix matrix = new SMatrix(a.arr.GetLength(0) - b.arr.GetLength(0) + 1, a.arr.GetLength(1) - b.arr.GetLength(1) + 1);

            for (int i = 0; i < a.arr.GetLength(0) - b.arr.GetLength(0) +1 ; i++)
            {
                for (int y = 0; y < a.arr.GetLength(1) - b.arr.GetLength(1) + 1; y++)
                {
                    int summe = 0;

                    for (int xi = 0; xi < b.arr.GetLength(0); xi++)
                    {
                        for (int yi = 0; yi < b.arr.GetLength(1); yi++)
                        {
                            summe += a.arr[i + xi, y + yi] * b.arr[xi, yi];
                        }
                    }
                    matrix.arr[i, y] = summe;
                }
            }

            return matrix;
        }
    }
}
