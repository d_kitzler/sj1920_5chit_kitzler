﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RechnungPDFGenerator
{
    class Zahlungskonditionen
    {
        public int Prozent { get; set; }
        public int Tage { get; set; }
        public int TageNetto { get; set; }

        public int ID { get; set; }
    }
}
