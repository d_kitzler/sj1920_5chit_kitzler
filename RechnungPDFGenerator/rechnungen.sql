-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 22. Mrz 2020 um 14:02
-- Server-Version: 10.1.36-MariaDB
-- PHP-Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `rechnungen`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `artikel`
--

CREATE TABLE `artikel` (
  `ID` int(11) NOT NULL,
  `preis` int(11) NOT NULL,
  `Bezeichnung` varchar(255) NOT NULL,
  `Anzahl` int(11) NOT NULL,
  `Einheit` varchar(100) NOT NULL,
  `Rabatt` int(11) NOT NULL,
  `RechnungID` int(11) NOT NULL,
  `Ust` int(11) NOT NULL,
  `Datum` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `artikel`
--

INSERT INTO `artikel` (`ID`, `preis`, `Bezeichnung`, `Anzahl`, `Einheit`, `Rabatt`, `RechnungID`, `Ust`, `Datum`) VALUES
(1, 700, 'Notebook ACER E5699', 1, 'Stk.', 10, 1, 13, '2020-01-01'),
(2, 500, 'iPhone XR', 5, 'Stk.', 50, 1, 20, '2020-01-01'),
(3, 99, 'Airpods Pro', 10, 'Stk.', 10, 1, 13, '2020-01-01'),
(4, 400, 'Samsung SMART TV', 10, 'Stk.', 2, 1, 20, '2020-01-01'),
(5, 200, 'Tastatur Raccer', 2, 'Stk.', 2, 1, 13, '2020-03-22'),
(6, 50, 'Maus Roccat', 2, 'Stk', 2, 1, 20, '2020-03-22'),
(7, 100, 'Router XXL', 2, 'Stk.', 2, 1, 13, '2020-03-22'),
(8, 40, 'Mousepad Steelseries', 2, 'Stk.', 4, 1, 20, '2020-03-22'),
(9, 500, 'Desktop PC', 2, 'Stk.', 2, 1, 20, '2020-03-22'),
(10, 50, 'HDD Samsung 100 TB', 2, 'Stk.', 2, 1, 13, '2020-03-22'),
(11, 70, 'Motherboard Intel', 5, 'Stk.', 7, 1, 13, '2020-03-22'),
(12, 20, 'HDMI Kabel', 20, 'Stk.', 2, 1, 20, '2020-03-22'),
(13, 150, 'Nvidia GFORCE GTX 1060', 9, 'Stk.', 6, 1, 13, '2020-03-22'),
(14, 50, 'Gehäuse mit LED', 4, 'Stk.', 7, 1, 13, '2020-03-22'),
(15, 70, 'Gehäuse ohne LED', 7, 'Stk', 7, 1, 13, '2020-03-22'),
(16, 80, 'Lüfter', 6, 'Stk.', 20, 1, 13, '2020-03-22'),
(17, 99, 'Ladekabel Laptop', 2, 'Stk.', 8, 1, 13, '2020-03-22'),
(18, 150, 'WLAN Kabel', 2, 'Stk.', 20, 1, 13, '2020-03-22'),
(19, 100, 'Headset Razer', 8, 'Stk.', 12, 1, 13, '2020-03-22'),
(20, 50, 'Wireless Headset', 2, 'Stk.', 9, 1, 20, '2020-03-22'),
(21, 600, 'Samsung Galaxy S10', 2, 'Stk.', 5, 1, 20, '2020-03-22');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `firma`
--

CREATE TABLE `firma` (
  `ID` int(11) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `Straße` varchar(100) NOT NULL,
  `PLZ` int(11) NOT NULL,
  `Ort` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `firma`
--

INSERT INTO `firma` (`ID`, `Name`, `Straße`, `PLZ`, `Ort`) VALUES
(1, 'David Kitzler', 'Südhangstraße 35/7', 3910, 'Zwettl');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `kunde`
--

CREATE TABLE `kunde` (
  `ID` int(11) NOT NULL,
  `Vorname` varchar(50) NOT NULL,
  `Nachname` varchar(50) NOT NULL,
  `Straße` varchar(100) NOT NULL,
  `PLZ` int(11) NOT NULL,
  `Ort` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `kunde`
--

INSERT INTO `kunde` (`ID`, `Vorname`, `Nachname`, `Straße`, `PLZ`, `Ort`) VALUES
(1, 'Niklas', 'Pettrich', 'Wagramerstraße 13', 1220, 'Wien'),
(2, 'Niklas ', 'Wandl', 'Brand 121', 3910, 'Zwettl'),
(3, 'Sebastian ', 'Penz', 'Huberstraße 23', 1150, 'Wien');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `rechnung`
--

CREATE TABLE `rechnung` (
  `ID` int(11) NOT NULL,
  `Name` varchar(200) NOT NULL,
  `BeginnText` varchar(255) NOT NULL,
  `EndeText` varchar(255) NOT NULL,
  `Rabatt` int(11) NOT NULL,
  `KundenID` int(11) NOT NULL,
  `FirmenID` int(11) NOT NULL,
  `ZKID` int(11) NOT NULL,
  `FooterText` varchar(255) NOT NULL,
  `Projekttext` varchar(255) NOT NULL,
  `Sachbearbeiter` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `rechnung`
--

INSERT INTO `rechnung` (`ID`, `Name`, `BeginnText`, `EndeText`, `Rabatt`, `KundenID`, `FirmenID`, `ZKID`, `FooterText`, `Projekttext`, `Sachbearbeiter`) VALUES
(1, 'Rechnung', 'Hier finden Sie ihre Informationen über ihre gekauften Artikeln.', 'Wir danken für Ihr Vertrauen und hoffen, Sie bald wieder als Kunden begrüßen zu dürfen', 10, 1, 1, 1, 'David Kitzler| IBAN: AT99 1337 9999 9999 | Telefon: 02814/13459', 'Rechnung zu den verschiedenen Artikeln', 'Christian Huber');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `rechnungen_artikel`
--

CREATE TABLE `rechnungen_artikel` (
  `ArtikelID` int(11) NOT NULL,
  `RechnungID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `rechnungen_artikel`
--

INSERT INTO `rechnungen_artikel` (`ArtikelID`, `RechnungID`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `zahlungskonditionen`
--

CREATE TABLE `zahlungskonditionen` (
  `ID` int(11) NOT NULL,
  `Prozent` int(11) NOT NULL,
  `Tage` int(11) NOT NULL,
  `TageNetto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `zahlungskonditionen`
--

INSERT INTO `zahlungskonditionen` (`ID`, `Prozent`, `Tage`, `TageNetto`) VALUES
(1, 10, 8, 20);

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `artikel`
--
ALTER TABLE `artikel`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `RechnungID` (`RechnungID`);

--
-- Indizes für die Tabelle `firma`
--
ALTER TABLE `firma`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `kunde`
--
ALTER TABLE `kunde`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `rechnung`
--
ALTER TABLE `rechnung`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `FirmenID` (`FirmenID`),
  ADD KEY `fk_Rechnung_ZKID` (`ZKID`),
  ADD KEY `KundenID` (`KundenID`);

--
-- Indizes für die Tabelle `rechnungen_artikel`
--
ALTER TABLE `rechnungen_artikel`
  ADD KEY `ArtikelID` (`ArtikelID`),
  ADD KEY `RechnungID` (`RechnungID`);

--
-- Indizes für die Tabelle `zahlungskonditionen`
--
ALTER TABLE `zahlungskonditionen`
  ADD PRIMARY KEY (`ID`);

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `artikel`
--
ALTER TABLE `artikel`
  ADD CONSTRAINT `fk_Artikel_RechnungID` FOREIGN KEY (`RechnungID`) REFERENCES `rechnung` (`ID`);

--
-- Constraints der Tabelle `rechnung`
--
ALTER TABLE `rechnung`
  ADD CONSTRAINT `fk_Rechnung_FirmenID` FOREIGN KEY (`FirmenID`) REFERENCES `firma` (`ID`),
  ADD CONSTRAINT `fk_Rechnung_ZKID` FOREIGN KEY (`ZKID`) REFERENCES `zahlungskonditionen` (`ID`),
  ADD CONSTRAINT `rechnung_ibfk_1` FOREIGN KEY (`KundenID`) REFERENCES `kunde` (`ID`);

--
-- Constraints der Tabelle `rechnungen_artikel`
--
ALTER TABLE `rechnungen_artikel`
  ADD CONSTRAINT `rechnungen_artikel_ibfk_1` FOREIGN KEY (`ArtikelID`) REFERENCES `artikel` (`ID`),
  ADD CONSTRAINT `rechnungen_artikel_ibfk_2` FOREIGN KEY (`RechnungID`) REFERENCES `rechnung` (`ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
