﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COREFEATURES
{
    class Program
    {
        static void Main(string[] args)
        {         
            Schueler elvis = null;
            string klasse = elvis?.Klasse; //wird überprüft ob es auf null ist, bevor auf die Eigenschaft zugegriffen wird.

            Schueler david = new Schueler();
            Console.WriteLine(david.ToString());

            //Pattern Matching
            switch (david)
            {
                case Schueler s when (s.Klasse == "5CHIT"):
                    Console.WriteLine("Dieser Schueler geht in die 5. Klasse");
                    break;
                case Schueler s when (s.Klasse == "4CHIT"):
                    Console.WriteLine("Dieser Schueler geht in die 4. Klasse");
                    break;
                case Schueler s when (s.Klasse == "3CHIT"):
                    Console.WriteLine("Dieser Schueler geht in die 3. Klasse");
                    break;
                case Schueler s when (s.Klasse == "2CHIT"):
                    Console.WriteLine("Dieser Schueler geht in die 2. Klasse");
                    break;
                case Schueler s when (s.Klasse == "1CHIT"):
                    Console.WriteLine("Dieser Schueler geht in die 1. Klasse");
                    break;
                case null:
                    throw new ArgumentNullException(nameof(david)); //nameof
                default:
                    Console.WriteLine("Dieser Schueler ist nicht zugeordnet");
                    break;
            }

            //Tuble Example
            int[] values = { 1, 2, 3, 4, 5, 6, 7, 8 };

            var (sum, count) = Numbers(values);

            Console.WriteLine($"Sum: {sum}, Count: {count}");

            //Exception Filter
            string filename = "";

            try
            {
                var datei = System.IO.File.ReadLines(filename);
            }

            catch(ArgumentException) when (filename == "")
            {
                Console.WriteLine("Kein Datename angegeben!");
            }

            catch (ArgumentException ex) when (ex.Message.Contains("Illegales"))
            {
                Console.WriteLine("Ungültige Zeichen im Dateinamen: " + filename);
            }

            Console.ReadLine();
        }

        //Tuples
        static (int sum, int count) Numbers(int[] values)
        {
            var r = (sum: 0, count: 0);

            foreach(var v in values)
            {
                LocalFunction(v, 1);
            }

            //Local Functions
            void LocalFunction(int s, int c) { r.sum += s; r.count += c;  }
            return r;
        }

    }
}
