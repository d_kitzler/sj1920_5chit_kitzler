﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RechnungPDFGenerator
{
    class Kunde
    {
        public string Nachname { get; set; }
        public string Ort { get; set; }
        public string Strasse { get; set; }
        public string Vorname { get; set; }
        public int PLZ { get; set; }

        public int ID { get; set; }
    }
}
