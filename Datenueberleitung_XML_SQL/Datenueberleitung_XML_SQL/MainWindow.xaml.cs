﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;

namespace Datenueberleitung_XML_SQL
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string fileName;
        public MainWindow()
        {
            InitializeComponent();
          
        }

        private void openButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();

            openFileDialog.DefaultExt = "xml";
            openFileDialog.Filter = "XML Files (*.xml)|*.xml";

            openFileDialog.ShowDialog();

            fileName = openFileDialog.FileName.ToString();

            ReadIn(fileName);
        }

        private void ReadIn(string fileName)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(fileName);
            XmlNode root = doc.SelectSingleNode("Liste");

            //Anzahl der Patienten
            int anzahlPatienten = root.SelectNodes("Name").Count;
            tb1.Text = anzahlPatienten.ToString();

            // Anzahl aller durchgeführten Behandlungen
            int anzahlBehandlungen = root.SelectNodes("Name/Behandlungen/Behandlung").Count;
            tb2.Text = anzahlBehandlungen.ToString();

            //Anzahl unterschiedlicher Behandlungen
            List<string> unterschiedlicheBehandlungen = new List<string>();
            foreach(XmlNode n in root.SelectNodes("Name/Behandlungen/Behandlung/Bezeichnung"))
            {
                if (!unterschiedlicheBehandlungen.Contains(n.InnerText))
                    unterschiedlicheBehandlungen.Add(n.InnerText);
            }
            tb3.Text = unterschiedlicheBehandlungen.Count.ToString();

            //Anzahlder Patienten mit einer Behandlung, Anzahl der Patienten mit zwei Behandlungen, Anzahl der Patienten mit mehr als zwei Behandlungen
            int anzahlEineBehandlung = 0;
            int anzahlZweiBehandlungen = 0;
            int anzahlMehrAlsZweiBehandlungen = 0;

            foreach(XmlNode n in root.SelectNodes("Name"))
            {
                int counter = 0;

                foreach (XmlNode nTwo in n.SelectNodes("Behandlungen/Behandlung"))
                {
                    counter++;
                }

                if (counter == 1)
                {
                    anzahlEineBehandlung++;
                }

                else if (counter == 2)
                {
                    anzahlZweiBehandlungen++;
                }

                else if (counter > 2)
                {
                    anzahlMehrAlsZweiBehandlungen++;
                }
            }
            
            tb4.Text = anzahlEineBehandlung.ToString();
            tb5.Text = anzahlZweiBehandlungen.ToString();
            tb6.Text = anzahlMehrAlsZweiBehandlungen.ToString();

            //Summe der Therapien
            int summeTherapieZeit = 0;
            foreach(XmlNode n in root.SelectNodes("Name/Behandlungen/Behandlung/Dauer"))
            {
                summeTherapieZeit += Convert.ToInt32(n.InnerText);
            }
            tb7.Text = summeTherapieZeit.ToString();


        }

        private void DeleteDatabase()
        {
            string cs = @"Provider = Microsoft.ACE.OLEDB.12.0; Data Source = Transfer.accdb; Persist Security Info = False;";

            OleDbConnection con = new OleDbConnection(cs);
            con.Open();
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = con;
            cmd.CommandText = "delete from Patient";
            cmd.ExecuteNonQuery();
            cmd.CommandText = "delete from Behandlung";
            cmd.ExecuteNonQuery();
            cmd.CommandText = "delete from PatientBehandlungen";
            cmd.ExecuteNonQuery();
            con.Close();
        }

        private void WriteSQL()
        {
            XmlDocument document = new XmlDocument();
            document.Load(fileName);
            XmlDocument doc = new XmlDocument();
            doc.LoadXml("<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + document.InnerXml);
            XmlNode root = doc.SelectSingleNode("Liste");

            string cs = @"Provider = Microsoft.ACE.OLEDB.12.0; Data Source = Transfer.accdb; Persist Security Info = False;";
            OleDbConnection connection = new OleDbConnection(cs);
            connection.Open();
            OleDbCommand command = new OleDbCommand();
            command.Connection = connection;

            int counter = 0;
            List<string> unterschiedlicheBehandlungen = new List<string>();
            foreach (XmlNode n in root.SelectNodes("Name/Behandlungen/Behandlung/Bezeichnung"))
            {
                if (!unterschiedlicheBehandlungen.Contains(n.InnerText))
                    unterschiedlicheBehandlungen.Add(n.InnerText);
            }

            foreach(string anzahl in unterschiedlicheBehandlungen)
            {
                counter++;
                command.CommandText = "insert into Behandlung (ID,Bezeichnung) values ('" + counter.ToString() + "','" + anzahl + "')";
                command.ExecuteNonQuery();
            }

            int counter_ = 0;
            foreach(XmlNode node in root.SelectNodes("Name"))
            {
                counter_++;

                command.CommandText = "insert into Patient (ID, Nachname, Vorname, SVNR) values ('" + counter_.ToString() + "','" + node.SelectSingleNode("Nachname").InnerText + "', '" + node.SelectSingleNode("Vorname").InnerText + "', '" + node.SelectSingleNode("SV").InnerText + "')";
                command.ExecuteNonQuery();

                foreach (XmlNode node2 in node.SelectNodes("Behandlungen/Behandlung"))
                {
                    string dauer = node2.SelectSingleNode("Dauer").InnerText;
                    string datum = node2.SelectSingleNode("Datum").InnerText;
                    string bezeichnung = node2.SelectSingleNode("Bezeichnung").InnerText;

                    command.CommandText = "insert into PatientBehandlungen (PatientID, BehandlungsID, Dauer, Datum) values ('" + counter_.ToString() + "', '" + (Convert.ToInt32(unterschiedlicheBehandlungen.FindIndex(s => s == bezeichnung).ToString()) + 1).ToString() + "', '" + dauer + "', '" + datum + "')";
                    command.ExecuteNonQuery();
                }
            }

            connection.Close();
        }

        private string selectPatientBehandlungen()
        {
            string cs = @"Provider = Microsoft.ACE.OLEDB.12.0; Data Source = Transfer.accdb; Persist Security Info = False;";

            OleDbConnection connection = new OleDbConnection(cs);
            connection.Open();
            OleDbCommand command = new OleDbCommand();
            command.Connection = connection;
            command.CommandText = "select * from patientbehandlungen";
            OleDbDataReader reader = command.ExecuteReader();
            StringBuilder patientBehandlungen = new StringBuilder();

            while(reader.Read())
            {
                patientBehandlungen.Append(reader[0] + ";" + reader[1] + ";" + reader[2] + ";" + reader[3] + "\n");
            }

            reader.Close();
            connection.Close();
            return patientBehandlungen.ToString();
        }

        private string selectBehandlungen()
        {
            string cs = @"Provider = Microsoft.ACE.OLEDB.12.0; Data Source = Transfer.accdb; Persist Security Info = False;";

            OleDbConnection connection = new OleDbConnection(cs);
            connection.Open();
            OleDbCommand command = new OleDbCommand();
            command.Connection = connection;
            command.CommandText = "select * from behandlung";
            OleDbDataReader reader = command.ExecuteReader();
            StringBuilder behandlungen = new StringBuilder();

            while (reader.Read())
            {
                behandlungen.Append(reader[0] + ";" + reader[1] + "\n");
            }

            reader.Close();
            connection.Close();
            return behandlungen.ToString();
        }

        private string selectPatienten()
        {
            string cs = @"Provider = Microsoft.ACE.OLEDB.12.0; Data Source = Transfer.accdb; Persist Security Info = False;";

            OleDbConnection connection = new OleDbConnection(cs);
            connection.Open();
            OleDbCommand command = new OleDbCommand();
            command.Connection = connection;
            command.CommandText = "select * from patient";
            OleDbDataReader reader = command.ExecuteReader();
            StringBuilder patienten = new StringBuilder();

            while (reader.Read())
            {
                patienten.Append(reader[0] + ";" + reader[1] + ";" + reader[2] + ";" + reader[3] + "\n");
            }

            reader.Close();
            return patienten.ToString();
        }

        private void transferButton_Click(object sender, RoutedEventArgs e)
        {
            DeleteDatabase();
            WriteSQL();

            int patienten = selectPatienten().Split('\n').Count() - 1;
            int behandlungen = selectPatientBehandlungen().Split('\n').Count() - 1;
            int verschiedeneBehanlungen = selectBehandlungen().Split('\n').Count() - 1;

            int anzahlEineBehandlung = 0;
            int anzahlZweiBehandlungen = 0;
            int anzahlMehrAlsZweiBehandlungen = 0;
            int summeTherapieZeit = 0;

            List<string> patientenID = new List<string>();
            List<string> patientenBehandlungen = selectPatientBehandlungen().Split('\n').ToList();
            patientenBehandlungen.RemoveAt(patientenBehandlungen.Count - 1);

            foreach (string s in patientenBehandlungen)
            {
                patientenID.Add(s.Split(';')[0]);
                summeTherapieZeit += Convert.ToInt32(s.Split(';')[2]);
            }

            for (int i = 0; i < patientenID.Count - 1; i++)
            {
                if (patientenID[i] == patientenID[i + 1])
                {
                    if (i < patientenID.Count - 2)
                    {
                        if (patientenID[i] == patientenID[i + 2])
                        {
                            i += 2;
                            anzahlMehrAlsZweiBehandlungen++;
                        }
                        else
                        {
                            i++;
                            anzahlZweiBehandlungen++;
                        }
                    }
                    else
                    {
                        i++;
                        anzahlZweiBehandlungen++;
                    }
                }
                else
                    anzahlEineBehandlung++;

            }

            tbsql1.Text = patienten.ToString();
            tbsql2.Text = behandlungen.ToString();
            tbsql3.Text = verschiedeneBehanlungen.ToString();
            tbsql4.Text = anzahlEineBehandlung.ToString();
            tbsql5.Text = anzahlZweiBehandlungen.ToString();
            tbsql6.Text = anzahlMehrAlsZweiBehandlungen.ToString();
            tbsql7.Text = summeTherapieZeit.ToString();

            if (tbsql1.Text == tb1.Text && tbsql2.Text == tb2.Text && tbsql3.Text == tb3.Text && tbsql4.Text == tb4.Text && tbsql5.Text == tb5.Text && tbsql6.Text == tb6.Text && tbsql7.Text == tb7.Text)
            {

                korrekt.Background = new SolidColorBrush(Colors.Green);
                korrekt.Text = "KORREKT!";
            }

            else
            {
                korrekt.Background = new SolidColorBrush(Colors.Red);
                korrekt.Text = "NICHT KORREKT!";
            }

        }
    }
}
