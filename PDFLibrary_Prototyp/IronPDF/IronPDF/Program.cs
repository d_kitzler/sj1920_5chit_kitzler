﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IronPdf;

namespace IronPDF
{
    class Program
    {
        static void Main(string[] args)
        {
           
            HtmlToPdf Renderer = new HtmlToPdf();
            Renderer.PrintOptions.PaperSize = PdfPrintOptions.PdfPaperSize.A4;
            Renderer.PrintOptions.PaperOrientation = PdfPrintOptions.PdfPaperOrientation.Portrait;

            Renderer.PrintOptions.Footer = new SimpleHeaderFooter() { CenterText = "Mayerhofer GmbH | IBAN: AT99 9999 9999 9999 | Telefon: 02814/13459", DrawDividerLine = true, RightText = "Seite {page} von {total-pages}" };

            PdfDocument doc = Renderer.RenderHtmlAsPdf("<h1> <b> Mayerhofer GmbH </b> " +
                "</h1> <img src='logo.png' height='200' width='370' style='margin-left:400px; margin-top:-105px'/>" + 
                " <hr> </hr>");

           
            var OutputPath = "test.pdf";

            doc.SaveAs(OutputPath);
            System.Diagnostics.Process.Start(OutputPath);

        }
    }
}
