﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;

namespace HangMan
{
    /// <summary>
    /// This is the class "WordProvider". This class is responsible for the procurement of the different words.
    /// </summary>
    public class WordProvider
    {
        private List<string> words;
        private static WordProvider instance_;
        private string file;

        protected WordProvider(string file)
        {
            this.file = file;

            words = new List<string>();
            ReadWords();
        }    
        public string GetWord()
        {
            Random randy = new Random();
            return words[randy.Next(0, words.Count)];
        }

        public static WordProvider GetInstance(string file)
        {
            if(instance_ == null)
            {
                instance_ = new WordProvider(file);
            }

            return instance_;
        }

        /// <summary>
        /// Here the words are read out of the XML file with the help of Regex. All words with A-Z or A-Z are taken!
        /// </summary>
        public void ReadWords()
        {
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(file);
            XmlNodeList word = xmlDocument.SelectNodes("Words/Word");
            Regex regex = new Regex("^[A-Za-z]+$");
            foreach (XmlNode node in word)
            {
                string word_ = node.InnerText;
                if (regex.Match(word_).Success)
                {
                    words.Add(node.InnerText.ToUpper());
                }           
            }
        }
    }
}