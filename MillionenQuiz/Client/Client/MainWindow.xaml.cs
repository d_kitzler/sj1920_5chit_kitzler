﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Client
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ClientClass client;
        Task task;

        public MainWindow()
        {
            InitializeComponent();

            try
            {
                client = new ClientClass();
                client.Service();
                client.Start();
            }

            catch(Exception e)
            {
                MessageBox.Show(e.Message);
            }
      
        }

        public async void GameStart()
        {
            try
            {
                await client.neuesSpiel();
            }

            catch(Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        private void b1_Click(object sender, RoutedEventArgs e)
        {
            if (task != null)
            {
                if (task.Status != TaskStatus.Running)
                {
                    task.Dispose();
                }
            }

            task = new Task(GameStart);
            task.Start();
            button1.Visibility = Visibility.Hidden;

            //while (!client.Ready)
            //{
            //    ;
            //}

            Fragen();
        }

        void Fragen()
        {
            try
            {
            lbQuestion.Dispatcher.BeginInvoke(new Action(() =>
            {
                lbQuestion.Content = client.frage.frage_;
            }));

            tbA1.Text = client.frage.frageliste[0];
            tbA2.Text = client.frage.frageliste[1];
            tbA3.Text = client.frage.frageliste[2];
            tbA4.Text = client.frage.frageliste[3];

            lbGameID.Dispatcher.BeginInvoke(new Action(() =>
            {
                lbGameID.Content = "GameID: " + client.GameID;
            }));

            lbLevel.Dispatcher.BeginInvoke(new Action(() =>
            {
                lbLevel.Content = "Level: " + client.Level;
            }));


            lbQuestion.Visibility = Visibility.Visible;
            tbA1.Visibility = Visibility.Visible;
            tbA2.Visibility = Visibility.Visible;
            tbA3.Visibility = Visibility.Visible;
            tbA4.Visibility = Visibility.Visible;
            lbGameID.Visibility = Visibility.Visible;
            lbLevel.Visibility = Visibility.Visible;
            tbA1.Background = new SolidColorBrush(Colors.AliceBlue);
            tbA2.Background = new SolidColorBrush(Colors.AliceBlue);
            tbA3.Background = new SolidColorBrush(Colors.AliceBlue);
            tbA4.Background = new SolidColorBrush(Colors.AliceBlue);
            btnExit.Visibility = Visibility;
            btnNext.Visibility = Visibility.Hidden;

            }

            catch(Exception e)
            {
                MessageBox.Show(e.Message);
            }

            finally
            {
                client.Ready = false;
            }

        }

        private async void btnExit_Click(object sender, RoutedEventArgs e)
        {
            await client.EndGame();

            while (!client.Ready);
            client.Stop();
            this.Close();
        }

        private async void tbA1_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (((SolidColorBrush)tbA1.Background).Color == Colors.AliceBlue && ((SolidColorBrush)tbA2.Background).Color == Colors.AliceBlue && ((SolidColorBrush)tbA3.Background).Color == Colors.AliceBlue && ((SolidColorBrush)tbA4.Background).Color == Colors.AliceBlue)
            {
                await client.Antwort(0);
                
                while (!client.Ready) ;
                CheckAnswers();
            }
        }

        private async void tbA2_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (((SolidColorBrush)tbA1.Background).Color == Colors.AliceBlue && ((SolidColorBrush)tbA2.Background).Color == Colors.AliceBlue && ((SolidColorBrush)tbA3.Background).Color == Colors.AliceBlue && ((SolidColorBrush)tbA4.Background).Color == Colors.AliceBlue)
            {
                await client.Antwort(1);

                while (!client.Ready) ;
                CheckAnswers();
            }
        }

        private async void tbA3_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (((SolidColorBrush)tbA1.Background).Color == Colors.AliceBlue && ((SolidColorBrush)tbA2.Background).Color == Colors.AliceBlue && ((SolidColorBrush)tbA3.Background).Color == Colors.AliceBlue && ((SolidColorBrush)tbA4.Background).Color == Colors.AliceBlue)
            {
                await client.Antwort(2);

                while (!client.Ready) ;
                CheckAnswers();
            }
        }

        private async void tbA4_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (((SolidColorBrush)tbA1.Background).Color == Colors.AliceBlue && ((SolidColorBrush)tbA2.Background).Color == Colors.AliceBlue && ((SolidColorBrush)tbA3.Background).Color == Colors.AliceBlue && ((SolidColorBrush)tbA4.Background).Color == Colors.AliceBlue)
            {
                await client.Antwort(3);

                while (!client.Ready) ;
                CheckAnswers();
            }
        }

        private void btnRestart_Click(object sender, RoutedEventArgs e)
        {
            b1_Click(sender, e);
            btnRestart.Visibility = Visibility.Hidden;
        }

        async void CheckAnswers()
        {
            client.Ready = false;
            try
            {
                if (client.AntwortStatus)
                {
                    if (client.AntwortID == 0)
                        tbA1.Background = new SolidColorBrush(Colors.Green);
                    else if (client.AntwortID == 1)
                        tbA2.Background = new SolidColorBrush(Colors.Green);
                    else if (client.AntwortID == 2)
                        tbA3.Background = new SolidColorBrush(Colors.Green);
                    else if (client.AntwortID == 3)
                        tbA4.Background = new SolidColorBrush(Colors.Green);

                    if (client.Level < 2)
                        btnNext.Visibility = Visibility.Visible;
                    if (client.Level == 2)
                    {
                        await client.EndGame();
                        while (!client.Ready) ;
                        MessageBox.Show("Gewonnen! - Start: " + client.Begin + "; Ende: " + client.Ende);
                        btnRestart.Visibility = Visibility.Visible;
                    }
                }
                else
                {
                    if (client.AntwortID == 0)
                        tbA1.Background = new SolidColorBrush(Colors.Red);
                    else if (client.AntwortID == 1)
                        tbA2.Background = new SolidColorBrush(Colors.Red);
                    else if (client.AntwortID == 2)
                        tbA3.Background = new SolidColorBrush(Colors.Red);
                    else if (client.AntwortID == 3)
                        tbA4.Background = new SolidColorBrush(Colors.Red);
                    await client.EndGame();
                    while (!client.Ready) ;
                    MessageBox.Show("Verloren! - Start: " + client.Begin + "; Ende: " + client.Ende);
                    btnRestart.Visibility = Visibility.Visible;
                }
            }
            catch (Exception e) { MessageBox.Show("Error" + e.Message); }
            finally { client.Ready = false; }
        }

        private async void btnNext_Click(object sender, RoutedEventArgs e)
        {
            if (client.Level < 2)
            {
                try
                {
                    await client.neueFrage();
                }
                catch { MessageBox.Show("Connection closed"); client.Stop(); this.Close(); }
                while (!client.Ready) ;
                Fragen();
            }
        }
    }
}
