﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MillionenQuiz
{
    class Program
    {
        static void Main(string[] args)
        {
            Server server = new Server();
            server.Start();
            Task task = new Task(server.Anfragen);
            task.Start();
            while (true)
            {
                Console.WriteLine("1 = Statistik ausgeben!");
                Console.WriteLine("E = Server end!");
                string command = Console.ReadLine();
                if (command == "E" || command == "e")
                {
                    try
                    {
                        server.Stop();
                    }
                    catch (Exception e) { Console.WriteLine("Server shuts down"); }
                    break;
                }
                if (command == "1")
                {
                    foreach (int k in Quiz.quizDic.Keys)
                    {
                        Quiz game = Quiz.quizDic[k];
                        Console.WriteLine("Game #" + k + " - Level " + game.Level + " erreicht!");
                        Console.WriteLine("Beginn: " + game.Start.ToShortTimeString());
                        Console.WriteLine();
                        foreach (Fragen q in game.Fragens)
                        {
                            Console.WriteLine("Frage: " + q.frage);
                            foreach (string a in q.antwort)
                            {
                                if (q.antwort.IndexOf(a) == q.richtig)
                                    Console.WriteLine("\t*" + a);
                                else
                                    Console.WriteLine("\t" + a);
                            }
                        }
                        Console.WriteLine("Ende: " + game.End.ToShortTimeString());
                        Console.WriteLine();
                    }
                }
            }
        }
    }
 }

