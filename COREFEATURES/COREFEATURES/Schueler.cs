﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COREFEATURES
{
    class Schueler
    {
        //Initializers for auto-properties
        public string Vorname { get; } = "David";
        public string Nachname { get; } = "Kitzler";
        public string Klasse { get; } = "5CHIT";

        //String interpolation & Expression-bodied properties
        public override string ToString() => $"{Vorname}, {Nachname}, {Klasse}";
           
    }
}
