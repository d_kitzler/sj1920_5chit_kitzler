﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Client
{
    class ClientClass
    {
        bool stop = false;

        public Frage frage = new Frage();
        public bool AntwortStatus { get; set; } = false;
        public int AntwortID { get; set; }
        public int GameID { get; set; }
        public int Level { get; set; }
        public bool Fertig { get; set; }
        public string Begin { get; set; }
        public string Ende { get; set; }
        public bool Ready { get; set; } = false;


        Task task;

        TcpClient client;

        NetworkStream stream;

        StreamReader streamreader;

        StreamWriter streamwriter;

        public ClientClass()
        {
            client = new TcpClient();
        }

        public void Service()
        {
            client.Connect(IPAddress.Parse("127.0.0.1"), 3333);
            stream = client.GetStream();
            task = new Task(getData);
        }

        public void Start()
        {
            task.Start();
        }

        public async Task neuesSpiel()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml("<REQUEST><ID>1</ID><GAMEID></GAMEID><ANSWERID></ANSWERID></REQUEST>");
            await sendDataAsync(doc.OuterXml);
        }

        public async Task neueFrage()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml("<REQUEST><ID>2</ID><GAMEID>" + GameID + "</GAMEID><ANSWERID></ANSWERID></REQUEST>");
            await sendDataAsync(doc.OuterXml);
        }

        public async Task Antwort(int antwortid)
        {
            AntwortID = antwortid;
            XmlDocument document = new XmlDocument();
            document.LoadXml("<REQUEST><ID>3</ID><GAMEID>" + GameID + "</GAMEID><ANSWERID>" + antwortid + "</ANSWERID></REQUEST>");
            await sendDataAsync(document.OuterXml);
        }

        public async Task EndGame()
        {
            XmlDocument document = new XmlDocument();
            document.LoadXml("<REQUEST><ID>4</ID><GAMEID>" + GameID + "</GAMEID><ANSWERID></ANSWERID></REQUEST>");
            await sendDataAsync(document.OuterXml);
        }


        public async Task sendDataAsync(string data)
        {
            await streamwriter.WriteLineAsync(data);
        }

        public void Stop()
        {
            stop = true;

            streamreader.Close();
            streamwriter.Close();
            stream.Close();
        }

        public void getData()
        {
            try
            {
                while (!stop)
                {
                    streamreader = new StreamReader(stream);
                    streamwriter = new StreamWriter(stream);

                    string data = "";
                    StringBuilder stringbuilder = new StringBuilder();

                    while (streamreader.Peek() != -1)
                    {
                        stringbuilder.Append(streamreader.ReadLine());
                    }

                    data = stringbuilder.ToString();

                    if (data != "")
                    {
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(data);
                        XmlNode root = doc.SelectSingleNode("RESPONSE");
                        XmlNode info = root.SelectSingleNode("ID");
                        string id = info.InnerText;
                        frage.frageliste.Clear();

                        if (id == "1")
                        {
                            XmlNode gid = root.SelectSingleNode("GAMEID");
                            GameID = Convert.ToInt32(gid.InnerText);
                            frage = new Frage();
                            frage.frageliste = new List<string>();
                            XmlNode lvl = root.SelectSingleNode("LEVEL");
                            Level = Convert.ToInt32(lvl.InnerText);
                            XmlNode q_ = root.SelectSingleNode("QUESTION");
                            frage.frage_ = q_.InnerText;
                            XmlNode answer1 = root.SelectSingleNode("ANSWER1");
                            frage.frageliste.Add(answer1.InnerText);
                            XmlNode answer2 = root.SelectSingleNode("ANSWER2");
                            frage.frageliste.Add(answer2.InnerText);
                            XmlNode answer3 = root.SelectSingleNode("ANSWER3");
                            frage.frageliste.Add(answer3.InnerText);
                            XmlNode answer4 = root.SelectSingleNode("ANSWER4");
                            frage.frageliste.Add(answer4.InnerText);
                        }

                        else if (id == "2")
                        {
                            XmlNode level = root.SelectSingleNode("LEVEL");
                            Level = Convert.ToInt32(level.InnerText);
                            XmlNode q_ = root.SelectSingleNode("QUESTION");
                            frage.frage_ = q_.InnerText;
                            XmlNode answer1 = root.SelectSingleNode("ANSWER1");
                            frage.frageliste.Add(answer1.InnerText);
                            XmlNode answer2 = root.SelectSingleNode("ANSWER2");
                            frage.frageliste.Add(answer2.InnerText);
                            XmlNode answer3 = root.SelectSingleNode("ANSWER3");
                            frage.frageliste.Add(answer3.InnerText);
                            XmlNode answer4 = root.SelectSingleNode("ANSWER4");
                            frage.frageliste.Add(answer4.InnerText);
                        }

                        else if (id == "3")
                        {
                            XmlNode correct = root.SelectSingleNode("CORRECT");
                            AntwortStatus = Convert.ToBoolean(correct.InnerText);
                        }

                        else if (id == "4")
                        {
                            XmlNode start = root.SelectSingleNode("BEGIN");
                            Begin = start.InnerText;
                            XmlNode end = root.SelectSingleNode("END");
                            Ende = end.InnerText;
                        }
                        Ready = true;
                    }
                }
            }

            catch (Exception e) { }

        }
    }
 }
