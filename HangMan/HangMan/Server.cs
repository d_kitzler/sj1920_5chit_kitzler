﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;

namespace HangMan
{
    /// <summary>
    /// This is the "server" class. Here are the methods for starting, stopping the server. 
    /// Also the methods Accept and Add Connection are included.
    /// </summary>
    public class Server
    {
        private string fileName;
        private TcpListener server;
        private List<Connection> connections;

        public Server(int port, string fileName)
        {
            server = new TcpListener(port);
            connections = new List <Connection>();

            this.fileName = fileName;
        }

        public List<Connection> Connection
        {
            get { return connections; }
            set { connections = value; }
        }

        /// <summary>
        /// Here the server is started
        /// </summary>
        public void Start()
        {
            server.Start();
        }

        /// <summary>
        /// Here the connections are addded!
        /// </summary>
        /// <param name="socket"></param>
        public void AddConnection(Socket socket)
        {
            Connection con = new Connection(this.fileName, socket);
        }

        /// <summary>
        /// Here the connections are accepted!
        /// </summary>
        public void AcceptConnection()
        {
            while(true)
            {
                Socket socket = server.AcceptSocket();
                AddConnection(socket);
            }
        }

        /// <summary>
        /// In this Method the Server is stopped!
        /// </summary>
        public void Stop()
        {

            foreach (Connection connection in connections)
            {
                connection.Stop();
            }

            server.Stop();   
        }
    }
}