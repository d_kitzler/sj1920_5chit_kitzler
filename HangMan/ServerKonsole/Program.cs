﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HangMan;

namespace ServerKonsole
{
    class Program
    {
        static void Main(string[] args)
        {
            int port = 0;
            bool isSuccessful = false;

            while (!isSuccessful)
            {
                Console.WriteLine("Geben Sie einen Port ein: ");
                if (Int32.TryParse(Console.ReadLine(), out port))
                {
                    isSuccessful = true;
                    Console.WriteLine("Server is running");
                }

                else
                {
                    Console.WriteLine("Kein gültiger Port");
                }
            }

            Server server = new Server(port, "words.xml");
            server.Start();
            Task task = new Task(server.AcceptConnection);
            task.Start();
            while(true)
            {
                Console.WriteLine("type stop to stop server");
                if(Console.ReadLine()=="stop")
                {
                    server.Stop();
                    break;
                }
            }
        }
    }
}
