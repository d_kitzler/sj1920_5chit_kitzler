﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IronPdf;
using MySql.Data.MySqlClient;

namespace RechnungPDFGenerator
{
    class Rechnung
    {
        public List<Artikel> artikelListe { get; set; }
        public string BeginText { get; set; }
        public string EndText { get; set; }
        public string FooterText { get; set; }
        public string Name { get; set; }
        public string Projekttext { get; set; }
        public double Rabatt { get; set; }
        public string Sachberater { get; set; }

        public int Nummer { get; set; }

        public Firma Firma
        {
            get; set;
        }

        public Zahlungskonditionen Zahlungskonditionen
        {
            get; set;
        }

        public Rechnung Rechnung1
        {
            get; set;
        }

        public Kunde Kunde
        {
            get; set;
        }
       
    }
}
