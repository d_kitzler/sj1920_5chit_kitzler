﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvolutionMatrix
{
    class RGBMatrix
    {
        public int [,,] arr { get; set; }

        public RGBMatrix(int x, int y, int z)
        {
            arr = new int[x, y, z];
        }

        public static SMatrix operator *(RGBMatrix a, SMatrix b)
        {
            SMatrix matrix = new SMatrix(a.arr.GetLength(0) - b.arr.GetLength(0) + 1, a.arr.GetLength(1) - b.arr.GetLength(1) + 1);

            for (int i = 0; i < a.arr.GetLength(0) - b.arr.GetLength(0) + 1; i++)
            {
                for (int y = 0; y < a.arr.GetLength(1) - b.arr.GetLength(1) + 1; y++)
                {
                    int summe = 0;

                    for (int xi = 0; xi < b.arr.GetLength(0); xi++)
                    {
                        for (int yi = 0; yi < b.arr.GetLength(1); yi++)
                        {
                            for (int z = 0; z < a.arr.GetLength(2); z++)
                            {
                                summe += a.arr[i + xi, y + yi,z] * b.arr[xi, yi];
                            }
                           
                        }
                    }
                    matrix.arr[i, y] = summe;
                }
            }

            return matrix;
        }
    }
}
